# OFC_Trainer

This is a simple Windows GUI I wrote using C++ and Qt in order to show my Pineapple Open Face Chinese Poker (OFC for short) solver at work. <br>
Hero plays vs two opponents, multiple ways to move cards (drag and drop, click source/destination), opponents think in parallel with the hero, normal and fantasy land plays implemented. <br>
Binary can be downloaded from the page I made at the time (http://ultimateofc.pythonanywhere.com/) <br>
GUI could be more polished if this was a commercial product, but for a few days work I am pretty happy how it turned out. It generated quite a bit of interest in the poker community. <br>
<br>
Click on the image to view a demo!
<br>
[![Watch the video](shot1.png)](https://www.youtube.com/watch?v=Mkxjb2dR7P8)

